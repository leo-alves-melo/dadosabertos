Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "DadosAbertos"
s.summary = "Dados Abertos do Brasil."
s.requires_arc = true

# 2
s.version = "1.0.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4
s.author = { "Leonardo Alves de Melo" => "leonardo.alves.melo.1995@gmail.com" }


s.homepage = "https://gitlab.com/leo-alves-melo/dadosabertos"

s.swift_version = "4.2"

s.source = { :git => "https://gitlab.com/leo-alves-melo/dadosabertos.git", :tag => "#{s.version}", :commit => "3c638276"}


# 7
s.framework = "UIKit"

# 8
s.source_files = "DadosAbertos/**/*.{swift}"

end
