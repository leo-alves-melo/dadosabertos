//
//  TRTaxService.swift
//  DadosAbertos
//
//  Created by Leonardo Alves de Melo on 02/10/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

/// Service to fetch the TR tax values in the open data
public class TRTaxService {

    /// Monthly TR tax API URL
    private static var serverUrl = "https://api.bcb.gov.br"

    private static let endPoint = "/dados/serie/bcdata.sgs.4174/dados?formato=json"

    /// Change the server for use webdservice
    public static func setServer(_ url: String) {
        serverUrl = url
    }

    /// Function that seeks the monthly values of the TR tax
    ///
    /// - Parameter completion: function receiving the monthly TR tax
    public static func allMonthlyTRTax(timeout: Int,
                                       completion: @escaping ([MonthlyTRTax], ErrorDadosAbertos?) -> Void) {

        guard let url = URL(string: serverUrl + endPoint) else {
            completion([], ErrorDadosAbertos.tryToReach)
            return
        }

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(timeout)
        configuration.timeoutIntervalForResource = TimeInterval(timeout)
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)

        let task = session.dataTask(with: url) { data, _, _ in
            if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []),
                let newData = json as? [[String: Any]] {

                var trs: [MonthlyTRTax] = []
                for json in newData {

                    if let dateString = json["data"] as? String {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        if let date = dateFormatter.date(from: dateString) {
                            if let valueString = json["valor"] as? String {
                                if let value = Float(valueString) {
                                    let taxa = MonthlyTRTax(date: date, value: value)
                                    trs.append(taxa)
                                }
                            }
                        }
                    }
                }
                completion(trs, nil)

            } else {
                DispatchQueue.main.async {
                    completion([], ErrorDadosAbertos.tryToReach)
                }
            }
        }
        task.resume()
    }
}
